#!/bin/bash
# The following script runs CERN CentOS functional tests with some tweaks for containerized context

yum install git -y

git clone https://gitlab.cern.ch/linuxsupport/cern_centos_functional_tests.git
cd cern_centos_functional_tests;

# Disable certain tests for docker base image, concat with defaults
cat >> ./skipped-tests.list  <<DELIM

7|tests/cern_basic/3_check_cern_ntp.sh|No NTP on base image
7|tests/cern_partitioning/1_part_labels.sh|No partitioning on docker images
7|tests/cern_partitioning/4_initrd.sh|No initrd on docker images
7|tests/cern_basic/5_check_ipv6.sh|Fails in docker
DELIM

./runtests.sh
